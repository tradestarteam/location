// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MySaveGame.h"
#include <functional>
#include "Kismet/GameplayStatics.h"
#include "Runtime/Online/HTTP/Public/Http.h"
#include "DummyActor.generated.h"

UCLASS()
class LOCATION_API ADummyActor : public AActor
{
	GENERATED_BODY()


public:

	float interpSpeed = 6.0f;
	FVector endLocation;
	FVector nextLocation;
	FHttpModule* Http;

	// Check for active UID
	bool isActive = false;

	ADummyActor(const class FObjectInitializer& ObjectInitializer);

	virtual void Tick(float DeltaTime) override;

	virtual void BeginPlay() override;

	UFUNCTION()
		void getLocationHTTP();

	UFUNCTION()
		void postLocationHTTP();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Variables")
		int32 pUID;

	void onLocationReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);

	int32 loadSavedPUID();

	void savePUID();

	UFUNCTION()
		void downloadUID();

	void onUIDReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);

	UFUNCTION()
		void confirmUID(int32 UID);

	void onUIDConfirmed(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);

};
