// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "MySaveGame.generated.h"

/**
 *
 */
UCLASS()
class LOCATION_API UMySaveGame : public USaveGame
{
	GENERATED_BODY()


public:

	UPROPERTY(VisibleAnywhere, Category = Basic)
		int32 playerUID;

	UPROPERTY(VisibleAnywhere, Category = Basic)
		FString slotName;

	UPROPERTY(VisibleAnywhere, Category = Basic)
		int32 userIndex;

	UMySaveGame();

};
