// Fill out your copyright notice in the Description page of Project Settings.

#include "DummyActor.h"

// load pUID from memory
int32 ADummyActor::loadSavedPUID()
{

	int32 returnVar = 0;
	try {
		UMySaveGame* saveGameInstance = Cast<UMySaveGame>(UGameplayStatics::CreateSaveGameObject(UMySaveGame::StaticClass()));
		saveGameInstance = Cast<UMySaveGame>(UGameplayStatics::LoadGameFromSlot(saveGameInstance->slotName, saveGameInstance->userIndex));
		if (saveGameInstance != NULL) {
			returnVar = saveGameInstance->playerUID;
		}
	}
	catch (...) {
	}
	return returnVar;
}

void ADummyActor::savePUID()
{
	try {
		// Save user info
		UMySaveGame* saveGameInstance = Cast<UMySaveGame>(UGameplayStatics::CreateSaveGameObject(UMySaveGame::StaticClass()));
		saveGameInstance->playerUID = pUID;
		UGameplayStatics::SaveGameToSlot(saveGameInstance, saveGameInstance->slotName, saveGameInstance->userIndex);
	}
	catch (...) {
	}
}


void ADummyActor::downloadUID()
{
	TSharedRef<IHttpRequest> Request = Http->CreateRequest();
	Request->OnProcessRequestComplete().BindUObject(this, &ADummyActor::onUIDReceived);
	Request->SetURL("http://127.0.0.1/unreal/getUID.php");
	Request->SetVerb("GET");
	Request->SetHeader(TEXT("User-Agent"), "X-UnrealEngine-Agent");
	Request->SetHeader("Content-Type", TEXT("application/json"));
	Request->ProcessRequest();
}

void ADummyActor::onUIDReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
	try {

		//Create a pointer to hold the json serialized data
		TSharedPtr<FJsonObject> JsonObject;

		//Create a reader pointer to read the json data
		TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(Response->GetContentAsString());

		//Deserialize the json data given Reader and the actual object to deserialize
		if (FJsonSerializer::Deserialize(Reader, JsonObject))
		{
			int32 receivedUID = JsonObject->GetNumberField("playerUID");
			GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Orange, TEXT("Received UID: ") + FString::FromInt(receivedUID));

			confirmUID(receivedUID);

		}
	}
	catch (...) {
		// Show debug error
	}
}

void ADummyActor::confirmUID(int32 UID)
{
	TSharedRef<IHttpRequest> Request = Http->CreateRequest();
	Request->OnProcessRequestComplete().BindUObject(this, &ADummyActor::onUIDConfirmed);
	Request->SetURL("http://127.0.0.1/unreal/saveUID.php");
	Request->SetVerb("POST");
	Request->SetHeader(TEXT("User-Agent"), "X-UnrealEngine-Agent");
	Request->SetHeader("Content-Type", TEXT("application/json"));
	FString json = "{ \"playerUID\" : " + FString::FromInt(UID) + "}";
	Request->SetContentAsString(json);
	Request->ProcessRequest();
}

void ADummyActor::onUIDConfirmed(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
	try {

		//Create a pointer to hold the json serialized data
		TSharedPtr<FJsonObject> JsonObject;

		//Create a reader pointer to read the json data
		TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(Response->GetContentAsString());

		//Deserialize the json data given Reader and the actual object to deserialize
		if (FJsonSerializer::Deserialize(Reader, JsonObject))
		{
			int32 confirmedUID = JsonObject->GetNumberField("playerUID");
			pUID = confirmedUID;
			GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Orange, TEXT("Confirmed pUID: ") + FString::FromInt(pUID));

			savePUID();

		}
	}
	catch (...) {
		// Show debug error
	}

}

// Sets default values
ADummyActor::ADummyActor(const class FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	Http = &FHttpModule::Get();
	endLocation = GetActorLocation();
}

// Updates user location on server in "realtime". Invoked around 100 times per second in my PC
void ADummyActor::postLocationHTTP()
{
	TSharedRef<IHttpRequest> Request = Http->CreateRequest();
	Request->SetURL("http://127.0.0.1/unreal/postLocation.php");
	Request->SetVerb("POST");
	Request->SetHeader(TEXT("User-Agent"), "X-UnrealEngine-Agent");
	Request->SetHeader("Content-Type", TEXT("application/json"));
	FString json = "{ \"PlayerUID\" : " + FString::FromInt(pUID) + ", \"X\" : " + FString::SanitizeFloat(GetActorLocation().X) + ", \"Y\" : " + FString::SanitizeFloat(GetActorLocation().Y) + ", \"Z\" : " + FString::SanitizeFloat(GetActorLocation().Z) + "}";
	Request->SetContentAsString(json);
	Request->ProcessRequest();
}

// Called on every tick around 100 times per second in my PC
void ADummyActor::getLocationHTTP()
{
	TSharedRef<IHttpRequest> Request = Http->CreateRequest();
	Request->OnProcessRequestComplete().BindUObject(this, &ADummyActor::onLocationReceived);
	Request->SetURL("http://127.0.0.1/unreal/getLocation.php");
	Request->SetVerb("GET");
	Request->SetHeader(TEXT("User-Agent"), "X-UnrealEngine-Agent");
	Request->SetHeader("Content-Type", TEXT("application/json"));
	Request->ProcessRequest();
}

// Called after location has been received from server. Invokes postLocationHTTP()
void ADummyActor::onLocationReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
	try {

		//Create a pointer to hold the json serialized data
		TSharedPtr<FJsonObject> JsonObject;

		//Create a reader pointer to read the json data
		TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(Response->GetContentAsString());

		//Deserialize the json data given Reader and the actual object to deserialize
		if (FJsonSerializer::Deserialize(Reader, JsonObject))
		{
			endLocation.X = JsonObject->GetNumberField("X");
			endLocation.Y = JsonObject->GetNumberField("Y");
			endLocation.Z = JsonObject->GetNumberField("Z");

			// Smooth interpolation to next point
			nextLocation = FMath::VInterpTo(GetActorLocation(), endLocation, GetWorld()->GetDeltaSeconds(), interpSpeed);

			// Change actor location to nextLocation
			SetActorLocation(nextLocation, false);

			// Upload location received to server
			postLocationHTTP();
			// GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Orange, endLocation.ToString());
		}
	}
	catch (...) {
		// Show debug error
	}
}



// Called every frame
void ADummyActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

//	if (isActive) {
		// Get coordinates from server and move actor towards it
		getLocationHTTP();
//	}


}

void ADummyActor::BeginPlay()
{
	PrimaryActorTick.bCanEverTick = false;

	pUID = loadSavedPUID();

	if (pUID == 0) {
		// Download and Save pUID
		downloadUID();
	}

	GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Green, TEXT("Player UID: ") + FString::FromInt(pUID));

	PrimaryActorTick.bCanEverTick = true;

	Super::BeginPlay();
}
